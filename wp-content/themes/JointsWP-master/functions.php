<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php');


// Remove 4.2 Emoji Support
// require_once(get_template_directory().'/assets/functions/disable-emoji.php');

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php');

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php');

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');

/**
 ** Activar el item de menú dependiendo de la url. Añade una clase al item activo
 **/

// Production
$baseBackUrl = get_site_url() . "/backoffice";
$baseApiUrl = get_site_url() . "/services";
$baseBackInternalUrl = get_site_url() . "/backoffice";
$baseApiInternalUrl = get_site_url() . "/services";

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class($classes, $item){
  // si es post o blog landing, es decir, si en la url está la palabra blog
  $blog_landing = strpos($_SERVER["REQUEST_URI"], 'blog');
  $es_blog = ( $blog_landing > 0 );

  if ($es_blog) {
    // activar el item de menú blog
    if($item->title == 'Blog'){
      $classes[] = 'current-menu-item active';
    }
  }
  return $classes;
}

add_action('wp_enqueue_scripts','load_backoffice_scripts');
add_action('admin_enqueue_scripts','load_backoffice_scripts');

function load_backoffice_scripts($hook){
	global $baseBackUrl;

	if ( 'toplevel_page_pamapam-new-entity' == $hook
			|| 'toplevel_page_pamapam-manage-entities' == $hook
			|| 'toplevel_page_pamapam-user-profile' == $hook) {
		wp_enqueue_style('vaadin-css', get_stylesheet_directory_uri() . '/assets/css/vaadin.css');
		wp_enqueue_script('vaadin-cors', get_stylesheet_directory_uri() . '/js/vaadin-cors.js');
		wp_enqueue_script('vaadin-bootstrap', $baseBackUrl . '/VAADIN/vaadinBootstrap.js', '', '', true);
		wp_enqueue_script('vaadin-init', get_stylesheet_directory_uri() . '/js/vaadin-init.js', '', '', true);
	}

	if ( 'toplevel_page_pamapam-new-entity' == $hook ) {
		$vaadinInitData = array(
				'browserDetailsUrl' => $baseBackUrl . '/ui/socialEconomyEntity',
				'baseBackUrl' => $baseBackUrl
		);
		wp_localize_script( 'vaadin-init', 'php_vars', $vaadinInitData);
	}
	if ( 'toplevel_page_pamapam-manage-entities' == $hook ) {
		$vaadinInitData = array(
				'browserDetailsUrl' => $baseBackUrl . '/ui/entitiesManagement',
				'baseBackUrl' => $baseBackUrl
		);
		wp_localize_script( 'vaadin-init', 'php_vars', $vaadinInitData);
	}
	if ( 'toplevel_page_pamapam-user-profile' == $hook ) {
		$vaadinInitData = array(
				'browserDetailsUrl' => $baseBackUrl . '/ui/userProfile',
				'baseBackUrl' => $baseBackUrl
		);
		wp_localize_script( 'vaadin-init', 'php_vars', $vaadinInitData);
	}
}

add_filter('body_class', 'add_special_body_class');

function add_special_body_class($classes) {
	if (is_page_template('template-backoffice.php')) {
		$classes [] = 'vaadin-body';
	}
	return $classes;
}

// Adds directori-landing-functions
require_once(get_template_directory().'/assets/functions/directori-landing-functions.php');

add_action('wp_ajax_entities_grid', 'entities_grid');
add_action('wp_ajax_nopriv_entities_grid', 'entities_grid');

// Adds directori-single-functions
require_once(get_template_directory().'/assets/functions/directori-single-functions.php');

add_action('wp_ajax_entity_single', 'entity_single');
add_action('wp_ajax_nopriv_entity_single', 'entity_single');

// Adds comunitat-landing-functions
require_once(get_template_directory().'/assets/functions/comunitat-landing-functions.php');

add_action('wp_ajax_user_grid', 'user_grid');
add_action('wp_ajax_nopriv_user_grid', 'user_grid');

// Adds comunitat-single-functions
require_once(get_template_directory().'/assets/functions/comunitat-single-functions.php');

add_action('wp_ajax_entity_single', 'user_single');
add_action('wp_ajax_nopriv_entity_single', 'user_single');

// Adds inici-map-functions
require_once(get_template_directory().'/assets/functions/inici-mapa-functions.php');

add_action('wp_ajax_entities_map', 'entities_map');
add_action('wp_ajax_nopriv_entities_map', 'entities_map');


function dashboard_redirect(){
	$author = wp_get_current_user();
	if (isset($author->roles [0])) {
		$current_role = $author->roles [0];
	} else {
		$current_role = 'no_role';
	}

	if ($current_role == 'xinxeta' || $current_role == 'external') {
		wp_redirect(admin_url('edit.php'));
	}
}

add_action('load-index.php','dashboard_redirect');

// ...	Remove items from admin menu
function remove_menus() {
	$author = wp_get_current_user();
	if (isset($author->roles [0])) {
		$current_role = $author->roles [0];
	} else {
		$current_role = 'no_role';
	}

	if ($current_role == 'xinxeta' || $current_role == 'external') {
		remove_menu_page('index.php'); // Dashboard
		remove_menu_page('wpcf7'); // Contact
		remove_menu_page('tools.php'); // Tools
		remove_menu_page('profile.php'); // Profile
		remove_menu_page('edit-comments.php'); // Comments
	}

	if ($current_role == 'registered') {
		remove_menu_page('edit.php'); // Posts
		remove_menu_page('index.php'); // Dashboard
		remove_menu_page('wpcf7'); // Contact
		remove_menu_page('tools.php'); // Tools
		remove_menu_page('profile.php'); // Profile
		remove_menu_page('edit-comments.php'); // Comments
	}
}

add_action('admin_menu', 'remove_menus');

// ...	Remove items from admin toolbar
function remove_toolbar_menus() {
	global $wp_admin_bar;

	$author = wp_get_current_user();
	if (isset($author->roles [0])) {
		$current_role = $author->roles [0];
	} else {
		$current_role = 'no_role';
	}

	if ($current_role == 'xinxeta' || $current_role == 'registered' || $current_role == 'external') {
		$wp_admin_bar->remove_menu( 'wp-logo' );
		$wp_admin_bar->remove_menu( 'comments' );
		$wp_admin_bar->remove_node( 'my-account' );
// 		$wp_admin_bar->remove_menu( 'edit-profile' );
// 		$wp_admin_bar->remove_menu( $author->nickname );
	}
}

add_action('wp_before_admin_bar_render', 'remove_toolbar_menus', 0);

function add_backoffice_menus() {
	$author = wp_get_current_user();
	if (isset($author->roles [0])) {
		$current_role = $author->roles [0];
	} else {
		$current_role = 'no_role';
	}

	if ($current_role == 'administrator') {
		add_menu_page(
				__('Àrea de gestió','pamapam-backoffice'),
				__('Àrea de gestió','pamapam-backoffice'),
				'create_pamapam_entity',
				'pamapam-backoffice',
				'add_external_vaadin_page',
				'dashicons-welcome-add-page');
	} else if ($current_role != 'registered') {
		add_menu_page(
			__('Crear nova fitxa','new-pamapam-entity'),
			__('Crear nova fitxa','new-pamapam-entity'),
			'create_pamapam_entity',
			'pamapam-new-entity',
			'add_vaadin_page',
			'dashicons-welcome-add-page');
		add_menu_page(
			__('Gestió de fitxes','manage-pamapam-entities'),
			__('Gestió de fitxes','manage-pamapam-entities'),
			'create_pamapam_entity',
			'pamapam-manage-entities',
			'add_vaadin_page',
			'dashicons-images-alt2');
		add_menu_page(
			__("Perfil d'usuari",'pamapam-user-profile'),
			__("Perfil d'usuari",'pamapam-user-profile'),
			'read',
			'pamapam-user-profile',
			'add_vaadin_page',
			'dashicons-admin-users');
	} else {
		add_menu_page(
			__("Perfil d'usuari",'pamapam-user-profile'),
			__("Perfil d'usuari",'pamapam-user-profile'),
			'read',
			'pamapam-user-profile',
			'add_vaadin_page',
			'dashicons-admin-users');
	}
}

function add_vaadin_page() {

	echo '<div id="content" class="vaadin-content">';
	echo '	<div id="inner-content" class="row vaadin-inner-content">';
	echo '		<main id="main" class="large-12 medium-12 columns vaadin-main" role="main">';
//	echo '			<div style="height: 650px; width: 100%; border: 2px solid green;" id="pamapam-backoffice" class="v-app">';
	echo '			<div style="width: 100%; background-color: #f1f1f1;" id="pamapam-backoffice" class="v-app">';
	echo '				<div class="v-app-loading"></div>';
	echo '				<noscript>[replaceable]You have to enable javascript in your browser to use an application built with Vaadin.</noscript>';
	echo '			</div>';
	echo '		</main>';
	echo '	</div>';
	echo '</div>';

}

function add_external_vaadin_page() {
	global $baseBackUrl;

	echo '<script> location.replace("' . $baseBackUrl . '/ui"); </script>';
}

add_action('admin_menu', 'add_backoffice_menus');

function disqus_embed($disqus_shortname) {
	if (is_single()) {
		global $post;
		wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
		echo '<div id="disqus_thread"></div>
		    <script type="text/javascript">
		        var disqus_shortname = "'.$disqus_shortname.'";
		        var disqus_title = "'.$post->post_title.'";
		        var disqus_url = "'.get_permalink($post->ID).'";
		        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
		    </script>';
	}
}

// ...	Disable Wordpress update notifications.
function hide_update_notice_to_all_but_admin_users()
{
	if (!current_user_can('update_core')) {
		remove_action( 'admin_notices', 'update_nag', 3 );
	}
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );

add_action('wp_logout', 'wpoa_end_logout', 0);

// ends the logout request by redirecting the user to the desired page:
function wpoa_end_logout() {
	$_SESSION["WPOA"]["RESULT"] = 'Logged out successfully.';
	unset($_SESSION["WPOA"]["LAST_URL"]);
	wp_safe_redirect(site_url());
	die();
}
//Add dot in string
function add3dots($string, $repl, $limit)
{
    if(strlen($string) > $limit)
    {
        return substr($string, 0, $limit) . $repl;
    }
    else
    {
        return $string;
    }
}

function addVesAlMapa (){
    $string = '<span class=link-to-map><a href="http://pamapam.org/ca" class="button">VES AL MAPA <span class="icon-ic_room_48px"></span></a></span>';
    return $string ;
}

add_action("wp_loaded", "redirect_iniciativa_form");

function redirect_iniciativa_form (){
	if ( isset( $_POST["iniciativa-submit"])){

	global $baseApiInternalUrl;
	$url = $baseApiInternalUrl . "/addInitiative";

	$name = isset($_POST ['f_name']) ? $_POST ['f_name'] : null;
	$nif = isset($_POST ['f_nif']) ? $_POST ['f_nif'] : null;
 	$province = isset($_POST ['f_province']) ? $_POST ['f_province'] : null;
	$region = isset($_POST ['f_region']) ? $_POST ['f_region'] : null;
	$town = isset($_POST ['f_town']) ? $_POST ['f_town'] : null;
	$district = isset($_POST ['f_district']) ? $_POST ['f_district'] : null;
	$neighborhood = isset($_POST ['f_neighborhood']) ? $_POST ['f_neighborhood'] : null;
	$openingHours = isset($_POST ['f_openingHours']) ? $_POST ['f_openingHours'] : null;
	$productTags = isset($_POST ['f_productTags']) ? $_POST ['f_productTags'] : null;
	$address = isset($_POST ['f_address']) ? $_POST ['f_address'] : null;
	$number = isset($_POST ['f_number']) ? $_POST ['f_number'] : null;
	$email = isset($_POST ['f_email']) ? $_POST ['f_email'] : null;
	$phone = isset($_POST ['f_phone']) ? $_POST ['f_phone'] : null;
	$web = isset($_POST ['f_web']) ? $_POST ['f_web'] : null;
	$twitter = isset($_POST ['f_twitter']) ? $_POST ['f_twitter'] : null;
	$facebook = isset($_POST ['f_facebook']) ? $_POST ['f_facebook'] : null;
	$googlePlus = isset($_POST ['f_googlePlus']) ? $_POST ['f_googlePlus'] : null;
	$instagram = isset($_POST ['f_instagram']) ? $_POST ['f_instagram'] : null;
	$pinterest = isset($_POST ['f_pinterest']) ? $_POST ['f_pinterest'] : null;
	$quitter = isset($_POST ['f_quitter']) ? $_POST ['f_quitter'] : null;
	$description = isset($_POST ['f_description']) ? $_POST ['f_description'] : null;
	$sector = isset($_POST ['f_sector']) ? $_POST ['f_sector'] : null;
	$legalForm = isset($_POST ['f_legalForm']) ? $_POST ['f_legalForm'] : null;
	$socialEconomyNetworks = isset($_POST ['f_socialEconomyNetworks[]']) ? $_POST ['f_socialEconomyNetworks[]'] : null;
	$collaboratingWith = isset($_POST ['f_collaboratingWith']) ? $_POST ['f_collaboratingWith'] : null;
	$socialBalance = isset($_POST ['socialBalance']) ? $_POST ['socialBalance'] : null;

	$data = array (
			'name' => $name,
			'nif' => $nif,
			'provinceId' => $province,
			'regionId' => $region,
			'townId' => $town,
			'districtId' => $district,
			'neighborhoodId' => $neighborhood,
			'openingHours' => $openingHours,
			'productTags' => $productTags,
			'labels' => $labels,
			'address' => $address . $number,
			'email' => $email,
			'phone' => $phone,
			'web' => $web,
			'twitter' => $twitter,
			'facebook' => $facebook,
			'googlePlus' => $googlePlus,
			'instagram' => $instagram,
			'pinterest' => $pinterest,
			'quitter' => $quitter,
			'description' => $description,
			'sectorId' => $sector,
			'legalFormId' => $legalForm,
			'socialEconomyNetworkIds' => $socialEconomyNetworks,
			'collaboratingWith' => $collaboratingWith,
			'socialBalance' => $socialBalance

	);
	$jsondata = json_encode(array_filter($data));
	$options = array (
			'http' => array (
					'header' => "Content-type: application/json",
					'method' => 'POST',
					'content' => $jsondata
			)
	);

	$context = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	wp_redirect(home_url('/ok-iniciativa'));
	exit();
	}
}

add_action("wp_loaded", "redirect_voluntaria_form");

function redirect_voluntaria_form (){
	if ( isset( $_POST["voluntaria-submit"])){
		global $baseApiInternalUrl;
    $url = $baseApiInternalUrl . "/addProposed";

    $name = isset($_POST ['f_name']) ? $_POST ['f_name'] : '';
    $province = isset($_POST ['f_province']) ? $_POST ['f_province'] : '';
    $region = isset($_POST ['f_region']) ? $_POST ['f_region'] : '';
    $town = isset($_POST ['f_town']) ? $_POST ['f_town'] : '';
    $district = isset($_POST ['f_district']) ? $_POST ['f_district'] : '';
    $neighborhood = isset($_POST ['f_neighborhood']) ? $_POST ['f_neighborhood'] : '';
    $address = isset($_POST ['f_address']) ? $_POST ['f_address'] : '';
    $number = isset($_POST ['f_number']) ? $_POST ['f_number'] : '';
    $description = isset($_POST ['f_description']) ? $_POST ['f_description'] : '';
    $proposerName = isset($_POST ['f_proposerName']) ? $_POST ['f_proposerName'] : '';
    $proposerEmail = isset($_POST ['f_proposerEmail']) ? $_POST ['f_proposerEmail'] : '';
    $comments = isset($_POST ['f_comments']) ? $_POST ['f_comments'] : '';


    $data = array (
            'name' => $name,
            'provinceId' => $province,
            'regionId' => $region,
            'townId' => $town,
            'districtId' => $district,
            'neighborhoodId' => $neighborhood,
            'address' => $address . ' ' . $number,
            'description' => $description,
            'proposerName' => $proposerName,
            'proposerEmail' => $proposerEmail,
            'comments' => $comments
    );

    $jsondata = json_encode($data);
    $options = array (
            'http' => array (
                    'header' => "Content-type: application/json",
                    'method' => 'POST',
                    'content' => $jsondata
            )
    );

    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    // $proposed_status = $result->status;
    // if ($proposed_status == 200) {

    wp_redirect(home_url('/ok-proposta'));
    exit();
}
}

add_action("wp_loaded", "redirect_xinxeta_form");
function redirect_xinxeta_form (){
	if ( isset( $_POST["xinxeta-submit"])){

global $baseApiInternalUrl;
$name = isset( $_POST['name'] ) ? $_POST['name'] : '';
$email = isset( $_POST['email'] ) ? $_POST['email'] : '';
$url = $baseApiInternalUrl ."/addUser";
$data = array(
    'name' => $name,
    'email' => $email
    );
$jsondata = json_encode($data);
$options = array(
        'http' => array(
        'header' => "Content-type: application/json",
        'method' => 'POST',
        'content' => $jsondata
                )
            );

$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);
wp_redirect(home_url('/ok-xinxeta'));
        exit();

}
}

add_action("wp_loaded", "redirect_butlleti_form");
function redirect_butlleti_form (){
	if ( isset( $_POST["butlleti-submit"])){

global $baseApiUrl;
$name = isset( $_POST['name'] ) ? $_POST['name'] : '';
$email = isset( $_POST['email'] ) ? $_POST['email'] : '';
$url = $baseApiUrl ."/addNewsletterRegistration";
$data = array(
    'name' => $name,
    'email' => $email
    );
$jsondata = json_encode($data);
$options = array(
        'http' => array(
        'header' => "Content-type: application/json",
        'method' => 'POST',
        'content' => $jsondata
                )
            );

$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);
wp_redirect(home_url('/ok-butlleti'));
    exit();

}
}

function directori_rewrite_tag() {
	add_rewrite_tag('%entityId%', '([^&]+)');
}
add_action('init', 'directori_rewrite_tag', 10, 0);

function directori_rewrite_rule() {
	add_rewrite_rule('directori/(.+)/?','index.php?page_id=83&entityId=$matches[1]','top');
	add_rewrite_rule('(ca|es)/directori/(.+)/?','index.php?page_id=83&entityId=$matches[2]','top');
}
add_action('init', 'directori_rewrite_rule', 10, 0);



// Bloqueig de les cookies de Google Analytics gestionat pel plugin EU Cookie Law
function blockGA(){
	echo '<script>';
  if ( function_exists('cookie_accepted') && cookie_accepted() ) {
	//cookies accepted
  } else {
	//cookies blocked
	echo "window['ga-disable-UA-57808927-1'] = true;";
  }
  echo '</script>';
}
add_action('wp_head', 'blockGA', 0, 0);

