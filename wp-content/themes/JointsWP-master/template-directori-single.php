<?php /* Template Name: SINGLE Directori*/ ?>

<?php
$url = $_SERVER ["REQUEST_URI"];
$entity_id = $wp_query->get('entityId');

$entity = call_entity();

if ($entity == null) {
	wp_redirect(home_url());
	exit();
}

$criteria = call_criteria();

function swap_seo_title(){
	global $entity;
	return "Pam a Pam | $entity->name";
}
add_filter("wpseo_opengraph_title", "swap_seo_title");
add_filter("wpseo_title", "swap_seo_title");
?>

<?php get_header(); ?>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
	integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ==" crossorigin=""></link>
<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
	integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log==" crossorigin=""></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/directori-single.js"></script>
<div id="content">
	<div id="inner-content" class="row">
		<div class="box-head">
			<span class=link-to-map><a href="<?php echo get_site_url(); ?>" class="button">VES AL MAPA</a></span>
		</div>

<?php
			entity_single();
?>
	</div>
	<div class="row">
		<div class="large-12 small-12 columns share">
			<div class="large-12 small-12 columns entry-content">
				Comparteix a les teves xarxes:
<?php
				echo do_shortcode("[wp_social_sharing social_options='facebook,twitter,googleplus,linkedin,pinterest' twitter_username='arjun077' facebook_text='Share on Facebook' twitter_text='Share on Twitter' googleplus_text='Share on Google+' linkedin_text='Share on Linkedin' pinterest_text='Share on Pinterest'  icon_order='f,t,g,l,p,x' show_icons='1' before_button_text='' text_position='' social_image='']");
?>
			</div>
		</div>
	</div>
	<!-- Punts del mateix sector -->
	<div class="row related">
		<p class="semi-bold">Iniciatives similars</p>
		<hr />
		<div id="punts" class="container">
<?php
			related_entities_sector($entity->id);
?>
    	</div>
	</div>
	<!-- Punts del mateix territori -->
	<div class="row related">
		<p class="semi-bold">Iniciatives properes</p>
		<hr>
		<div id="punts" class="container">
<?php
			related_entities_proximity($entity->id);
?>
		</div>
	</div>
</div>

<div class="contain-to-grid sand-bkg">
<?php
	get_template_part( 'parts/include', 'afiliation' );
?>
</div>

<?php get_footer(); ?>
