<?php

function user_grid() {
	global $baseApiInternalUrl;
	global $baseApiUrl;

	$page = empty($_POST['page']) ? 0 : $_POST['page'];
	$size = empty($_POST['size']) ? 8 : $_POST['size'];
	$name = empty($_POST['name']) ? null : $_POST['name'];
	$territoryId = $_POST['territoryId'];
	$territoryType = $_POST['territoryType'];
	$communityId = $_POST['communityId'];

	$data = array(
			'page' => $page,
			'size' => $size,
			'name' => $name,
			'territoryId' => $territoryId,
			'territoryType' => $territoryType,
			'communityId' => $communityId
	);
	$jsonData = json_encode($data);
	$postArray = array(
			'headers' => array('Content-Type' => 'application/json'),
			'body' => $jsonData
	);
	$postUrl = $baseApiInternalUrl . "/searchUsers";

	$entities_response = wp_remote_post($postUrl, $postArray);
	$entities_array = json_decode(wp_remote_retrieve_body($entities_response));

    $users = $entities_array->response->content;

	foreach ($entities_array->response->content as $key=>$user) {
	    if ($key % 4 == 0){
            echo '<div class="row" data-equalizer="users">';
        }
		user_box($user, $baseApiUrl);

		if ($key % 4 == 3){
	    	echo '</div>';
	    }
	}

	$count_users = count($users);

	if ( ($count_users % 4 > 0 ) && ($count_users % 4 != 3)){
		echo '</div>';
	}

    echo '<div class="panel clearfix"></div>';
	echo '<div id="entities-next-page">';
	echo '	<button id="more-contents" type="button" class="control control-text button" onclick="moreContents(' . strval((int) $page + 1) . ', ' . $size . ')">';
	echo '		VEURE MÉS';
	echo '	</button>';
	echo '</div>';

	if (wp_doing_ajax()) {
		die();
	}
}

function user_box($user, $baseApiUrl) {
	$userImageUrl = $user->pictureUrl ? $baseApiUrl . $user->pictureUrl : get_template_directory_uri().'/assets/images/xinxeta.png.png';
    $classImageUrl = $user->pictureUrl ? '' : 'xinxetaComunitat';

    echo '<div class="small-6 medium-3 large-3 columns panel">';
    echo '  <section class="featured-image" data-equalizer-watch="users" >';
    echo '    <div class="circle">';
    echo '      <a href="comunitat-article/'.$user->id.'"><img class="'.$classImageUrl.'" src="'.$userImageUrl.'" /></a>';
	echo '    </div>';
    echo '    <header class="article-header">';
    echo '      <h3 class="title"><a href="comunitat-article/'.$user->id.'">'.$user->name.' '.$user->surname.'</a></h3>';
	echo '    </header>';
    echo '    <hr />';
    echo '    <section class="entry-content description" itemprop="articleBody">';
    echo '        <p>';
    echo            add3dots($user->description,'...',50);
    echo '        </p>';
	echo '    </section>';
	echo '  </section>';
	echo '</div>';

}

?>