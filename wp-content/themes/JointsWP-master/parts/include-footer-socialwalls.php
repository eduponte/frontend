<!-- banners socials walls-->
  <div class="social-walls">

    <div class="row">
      <div class="large-6 small-12 columns">
      <h2 class="rubik">Facebook</h2>
	<script>(function(d, s, id) {
 	 var js, fjs = d.getElementsByTagName(s)[0];
 	 if (d.getElementById(id)) return;
  	js = d.createElement(s); js.id = id;
  	js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10";
  	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div class="fb-page" data-href="https://www.facebook.com/pamapamCAT/" data-tabs="timeline" data-width="640" data-height="380" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
	<blockquote cite="https://www.facebook.com/pamapamCAT/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pamapamCAT/">Pam a Pam</a></blockquote></div>
      </div>

      <div class="large-6 small-12 columns">
        <h2 class="rubik">Twitter</h2>
        <a class="twitter-timeline" data-height="380" data-width="640" href="https://twitter.com/pamapamcat"></a>
		<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
      </div>
   </div>

 </div> <!-- end socials walls -->
