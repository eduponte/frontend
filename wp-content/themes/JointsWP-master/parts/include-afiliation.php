<!-- banners afiliacions socials -->
  <div class="social-afiliations">

    <div class="row">
      <div class="large-6 small-12 columns">

        <div class="large-12 columns in">
          <div class="celito purple"></div>

          <p class="large-7 small-12 columns">segueix-nos</p>
          <ul class="large-5 small-12 columns">
            <li><a href="https://www.facebook.com/pamapamCAT/" target="_blank"><span class="icon-facebook"></span></a></li>
            <li><a href="https://twitter.com/pamapamcat?lang=es" target="_blank"><span class="icon-twitter"></span></a></li>
          </ul>
        </div>
      </div>
      <div class="large-6 small-12 columns">


        <div class="large-12 columns in">
          <div class="celito green"></div>

          <p class="large-7 small-12 columns">butlletí de notícies</p>
          <p class="large-5 small-12 columns"><a href="/butlleti/" class="">Apunta't <span class="icon-arrow-right"></span></a></p>
      </div>
      </div>
   </div>

 </div> <!-- end banners afiliacions socials -->
