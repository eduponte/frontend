<?php
/*
 * Template Name: LANDING Directori
 */
?>
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/sumoselect.css">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/jquery.sumoselect.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/directori-landing.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.js"></script>

<?php
function getSectors() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/mainSectors";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}
function getTerritories() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/territories";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}

$sectors = getSectors();
$territories = getTerritories()->response;
// Adjust the amount of rows in the grid
$grid_columns = 3;
?>

<div id="content">
	<div id="inner-content" class="row">
		<main id="main" class="large-12 medium-12 columns" role="main">
			<div class="box-head">
				<h1 class="page-title"><?php the_title(); ?></h1>
	            <?php echo addVesAlMapa(); ?>
	            <p><?php the_content(); ?></p>
			</div>
			<!-- Filtros y search. Mantener el form y las clases -->
			<div class="box-search">
				<div class="celito left green"></div>
				<div class="celito right orange"></div>
				<form id="searchEntitiesForm">
					<input type="submit" style="display: none" />
					<div class="row">
						<div class="large-4 columns">
							<select id="searchTerritory">
								<option id="0"></option><?php
								foreach($territories as $eachTerritory) {
	                            	$territoryId = $eachTerritory->id . '-' . $eachTerritory->type; ?>
	                            	<option id="<?php echo $territoryId; ?>" value="<?php echo $territoryId; ?>"><?php echo $eachTerritory->name; ?></option>
	                            <?php } ?>
							</select>
						</div>
						<div class="large-4 columns">
							<select id="searchSectors" multiple="multiple"><?php
								foreach ($sectors as $eachSector) {?>
									<option id="<?php echo $eachSector->id; ?>" value="<?php echo $eachSector->id; ?>"><?php echo $eachSector->name; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="large-4 columns txt-search">
								<input id="searchText" type="text" name="text" value="" placeholder="Cerca per text">
							<div>
								<div id="resultats"></div>
							</div>
						</div>

						<div class="large-12 columns">
							<div id="reset">
								<button type="reset" class="control control-text button">
									Tots<span class="icon-iconos-my-roca-comparer"></span>
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- Grilla de punts (establecimientos), no cambiar la clase -->
			<div id="punts" class="container">
	        </div>
		</main>
		<!-- end #main -->
	</div>
	<!-- end #inner-content -->
</div>
<!-- end #content -->
<div class="contain-to-grid sand-bkg">
	<?php get_template_part( 'parts/include', 'afiliation' ); ?>
</div>
<?php get_footer(); ?>
